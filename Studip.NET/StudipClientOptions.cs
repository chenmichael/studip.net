﻿using HtmlAgilityPack;
using System.Net;
using System.Text.RegularExpressions;

namespace Studip.NET;

public class StudipClientOptions {
    private const string DEFAULT_DOMAIN = "e-learning.tuhh.de";
    private const string LOGIN_URL = $"/studip/index.php?again=yes";

    /// <summary>
    /// Use <see cref="GetSessionCookie(string, string)"/> to get a session cookie.
    /// </summary>
    /// <param name="sessionCookie">Studip seminar sesson cookie.</param>
    public StudipClientOptions(string sessionCookie) => SessionCookie = sessionCookie;
    public static async Task<Cookie> GetSessionCookie(string username, string password, string domain = DEFAULT_DOMAIN, CancellationToken ct = default) {
        var cookieContainer = new CookieContainer();
        var baseUrl = new Uri($"https://{domain}");
        await CookieRequest(username, password, baseUrl, cookieContainer, ct);
        // This is necessary because somehow the server sends a second header that immediately deletes the before send cookie
        // Why the server does this? i have no clue
        // So now i just pick the first cookie and ignore the second deletion cookie
        return cookieContainer.GetAllCookies().FirstOrDefault(i => i.Name == StudipClient.SessionCookieName)
            ?? throw new HttpRequestException("Server did not respond with a session cookie!");
    }

    private static readonly Regex InputRgx = new(@"<[^>]*input\s+[^>]*name\s*=\s*""(?<key>[^""]*)""\s*[^>]*value\s*=\s*""(?<value>[^""]*)""[^>]*>", RegexOptions.Compiled);
    private static async Task CookieRequest(string username, string password, Uri baseUrl, CookieContainer cookieContainer, CancellationToken ct) {
        using var handler = new HttpClientHandler() { CookieContainer = cookieContainer };
        using var httpClient = new HttpClient(handler) { BaseAddress = baseUrl };
        var form = await GetLoginFormData(username, password, httpClient, ct);
        using var response = await httpClient.PostAsync(LOGIN_URL, new FormUrlEncodedContent(form), ct);
        var dom = await response.Content.ReadAsStringAsync(ct);
        var doc = new HtmlDocument();
        doc.LoadHtml(dom);
        if (doc.DocumentNode.SelectSingleNode("//*[@class[contains(.,'messagebox_error')]]")?.InnerText is string error)
            throw new HttpRequestException($"Login failed: {LessWhitespace(error)}");
    }

    private static readonly char[] WhiteSpace = new[] { ' ', '\t', '\n', '\r' };
    private static string LessWhitespace(string error) => string.Join(' ', error
        .Split(WhiteSpace, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries));

    private static async Task<Dictionary<string, string>> GetLoginFormData(string username, string password, HttpClient httpClient, CancellationToken ct) {
        using var response = await httpClient.GetAsync(LOGIN_URL, ct);
        response.EnsureSuccessStatusCode();
        var responseHtml = await response.Content.ReadAsStringAsync(ct);
        var values = InputRgx.Matches(responseHtml).ToDictionary(i => i.Groups["key"].Value, i => i.Groups["value"].Value);
        values["loginname"] = username;
        values["password"] = password;
        return values;
    }

    public string Domain { get; init; } = DEFAULT_DOMAIN;
    public string SessionCookie { get; init; }
    public string Schema { get; init; } = "https";
    public string RootUrl => $"{Schema}://{Domain}";
    public string JsonApiUrl => $"{RootUrl}{StudipClient.RequestBase}";
    public Func<HttpMessageHandler, HttpMessageHandler>? ConfigureMessageHandler { get; init; }
}