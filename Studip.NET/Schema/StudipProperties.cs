﻿using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

/// <summary>
/// Ausgewählte Konfigurationseinstellungen und Merkmale der Stud.IP-Installation werden mit diesem Schema abgebildet.
/// </summary>
[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class StudipProperties : JsonApiEntity {
    /// <summary>
    /// eine Beschreibung der Einstellung/des Merkmals
    /// </summary>
    [JsonProperty("description")] public string Description { get; set; } = default!;
    /// <summary>
    /// der Wert der Einstellung/des Merkmals
    /// </summary>
    [JsonProperty("value")] public string Value { get; set; } = default!;
    public override string ToString() => $"{Description} = {Value}";
}
