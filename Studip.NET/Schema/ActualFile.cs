﻿using System.Diagnostics;

namespace Studip.NET.Schema;

[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class ActualFile : JsonApiEntity { }