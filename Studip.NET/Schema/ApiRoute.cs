﻿using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

/// <summary>
/// Ausgewählte Konfigurationseinstellungen und Merkmale der Stud.IP-Installation werden mit diesem Schema abgebildet.
/// </summary>
[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class ApiRoute : JsonApiEntity {
    /// <summary>
    /// ein Vektor von HTTP-Verben wie GET, POST, PATCH und DELETE
    /// </summary>
    [JsonProperty("methods")] public Methods[] Methods { get; set; } = default!;
    /// <summary>
    /// ein URI-Pattern wie "/file-refs/{id}"
    /// </summary>
    [JsonProperty("pattern")] public string Pattern { get; set; } = default!;
    public override string ToString() => $"{Pattern} ({string.Join(", ", Methods)})";
}
