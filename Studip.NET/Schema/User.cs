﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class User : JsonApiEntity {
    /// <summary>
    /// der Nutzername
    /// </summary>
    [JsonProperty("username")] public string Username { get; set; } = default!;
    /// <summary>
    /// der formatierte Echtname des Nutzers
    /// </summary>
    [JsonProperty("formatted-name")] public string FormattedName { get; set; } = default!;
    /// <summary>
    /// der Nachname des Nutzers
    /// </summary>
    [JsonProperty("family-name")] public string FamilyName { get; set; } = default!;
    /// <summary>
    /// der Vorname des Nutzers
    /// </summary>
    [JsonProperty("given-name")] public string GivenName { get; set; } = default!;
    /// <summary>
    /// evtl. vorangestellte Titel
    /// </summary>
    [JsonProperty("name-prefix")] public string NamePrefix { get; set; } = default!;
    /// <summary>
    /// evtl. nachgestellte Titel
    /// </summary>
    [JsonProperty("name-suffix")] public string NameSuffix { get; set; } = default!;
    /// <summary>
    /// die globale Berechtigungsstufe
    /// </summary>
    [JsonProperty("permission")] public UserPermission Permission { get; set; } = default!;
    /// <summary>
    /// die E-Mail-Adresse des Nutzers
    /// </summary>
    [JsonProperty("email")] public string Email { get; set; } = default!;
    /// <summary>
    /// die Telefonnummer des Nutzers
    /// Die Sichtbarkeit des Attribute folgt den Sichtbarkeitseinstellungen, die der Nutzer vorgenommen hat.
    /// </summary>
    [JsonProperty("phone")] public string? Phone { get; set; }
    /// <summary>
    /// die URL der Homepage des Nutzers
    /// Die Sichtbarkeit des Attribute folgt den Sichtbarkeitseinstellungen, die der Nutzer vorgenommen hat.
    /// </summary>
    [JsonProperty("homepage")] public string? Homepage { get; set; }
    /// <summary>
    /// die private Adresse des Nutzers
    /// Die Sichtbarkeit des Attribute folgt den Sichtbarkeitseinstellungen, die der Nutzer vorgenommen hat.
    /// </summary>
    [JsonProperty("address")] public string? Address { get; set; }
    /// <summary>
    /// ein Link zum activity stream des Nutzers
    /// </summary>
    [JsonProperty("activitystream")] public Relationship ActivityStream { get; set; } = default!;
    /// <summary>
    /// die Blubber des Nutzers
    /// </summary>
    [JsonProperty("blubber-threads")] public Relationship BlubberThreads { get; set; } = default!;
    /// <summary>
    /// die Kontakte des Nutzers
    /// </summary>
    [JsonProperty("contacts")] public Relationship Contacts { get; set; } = default!;
    /// <summary>
    /// die Veranstaltungen der Nutzers (als Dozent)
    /// </summary>
    [JsonProperty("courses")] public Relationship Courses { get; set; } = default!;
    /// <summary>
    /// die Veranstaltungen der Nutzers (als Teilnehmer)
    /// </summary>
    [JsonProperty("course-memberships")] public Relationship CourseMemberships { get; set; } = default!;
    /// <summary>
    /// der Terminkalender des Nutzers
    /// </summary>
    [JsonProperty("events")] public Relationship Events { get; set; } = default!;
    [JsonProperty("file-refs")] public Relationship FileRefs { get; set; } = default!;
    [JsonProperty("folders")] public Relationship Folders { get; set; } = default!;
    [JsonProperty("inbox")] public Relationship Inbox { get; set; } = default!;
    /// <summary>
    /// die Institute der Nutzers
    /// </summary>
    [JsonProperty("institute-memberships")] public Relationship InstituteMemberships { get; set; } = default!;
    [JsonProperty("news")] public Relationship News { get; set; } = default!;
    [JsonProperty("Outbox")] public Relationship Outbox { get; set; } = default!;
    /// <summary>
    /// der Stundenplan des Nutzers
    /// </summary>
    [JsonProperty("schedule")] public Relationship Schedule { get; set; } = default!;
    [JsonProperty("links")] public Links Links { get; set; } = default!;
    [JsonProperty("meta")] public Meta Meta { get; set; } = default!;
    public override string ToString() => $"User {Username}: {FormattedName} ({Permission}) {Email}";
}
