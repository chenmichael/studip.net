﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class Semester : JsonApiEntity {
    [JsonProperty("title")] public string Title { get; set; } = default!;
    [JsonProperty("description")] public string Description { get; set; } = default!;
    [JsonProperty("start")] public DateTime Start { get; set; }
    [JsonProperty("end")] public DateTime End { get; set; }
    [JsonProperty("links")] public Links Links { get; set; } = default!;
    public override string ToString() => $"Semester {Title}: {Start.ToShortDateString()} - {End.ToShortDateString()}";
}