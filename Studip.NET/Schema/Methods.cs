﻿namespace Studip.NET.Schema;

public enum Methods {
    GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE, PATCH
}