﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

/// <summary>
/// Aus Nutzersicht sind Dateien in Stud.IP Ressourcen vom Typ "file-refs". Technisch gesehen sind es allerdings Verweise auf die Ressourcen vom Typ "files". Letztere sind die tatsächlich auf der Festplatte gespeicherten Dateien, die mithilfe der "file-refs" nur verlinkt werden.
/// Vereinfacht gesagt, hantiert man in der Regel immer mit "file-refs".
/// </summary>
[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class FileRef : JsonApiEntity, IFolderContained {
    /// <summary>
    /// der Name der Datei
    /// </summary>
    [JsonProperty("name")] public string FileName { get; set; } = default!;
    /// <summary>
    /// eine optionale Beschreibung der Datei
    /// </summary>
    [JsonProperty("description")] public string Description { get; set; } = default!;
    /// <summary>
    /// das Erstellungsdatum der Datei
    /// </summary>
    [JsonProperty("mkdate")] public DateTime CreationDate { get; set; }
    /// <summary>
    /// das Datum der letzten Änderung der Metadaten ('name', 'description', …) der Datei
    /// </summary>
    [JsonProperty("chdate")] public DateTime ModificationDate { get; set; }
    /// <summary>
    /// Wie häufig wurde diese Datei heruntergeladen?
    /// </summary>
    [JsonProperty("downloads")] public int Downloads { get; set; }
    /// <summary>
    /// die Größe der Datei in Byte
    /// </summary>
    [JsonProperty("filesize")] public int FileSize { get; set; }
    [JsonProperty("is-readable")] public bool IsReadable { get; set; }
    [JsonProperty("is-downloadable")] public bool IsDownloadable { get; set; }
    [JsonProperty("is-editable")] public bool IsEditable { get; set; }
    [JsonProperty("is-writable")] public bool IsWritable { get; set; }
    /// <summary>
    /// die tatsächliche Datei auf der Festplatte
    /// </summary>
    [JsonProperty("file")] public Relationship<JsonApiEntity> File { get; set; } = default!;
    /// <summary>
    /// der Nutzer, dem diese Datei gehört
    /// </summary>
    [JsonProperty("owner")] public Relationship<JsonApiEntity> Owner { get; set; } = default!;
    /// <summary>
    /// der Ordner, im dem diese Datei liegt
    /// </summary>
    [JsonProperty("parent")] public Relationship<JsonApiEntity>? Parent { get; set; } = default!;
    /// <summary>
    /// die Veranstaltung, die Einrichtung oder der Nutzer, in dessen Dateibereich diese Datei liegt
    /// </summary>
    [JsonProperty("range")] public Relationship<JsonApiEntity> Range { get; set; } = default!;
    /// <summary>
    /// die Lizenz, unter der diese Datei verfügbar gemacht wird
    /// </summary>
    [JsonProperty("terms-of-use")] public Relationship<JsonApiEntity> License { get; set; } = default!;
    [JsonProperty("meta")] public Meta Meta { get; set; } = default!;
    /// <summary>
    /// The content download url.
    /// </summary>
    [JsonIgnore] public string? DownloadUrl => Meta.GetValueOrDefault("download-url")?.ToObject<string>();
    public override string ToString() => $"File {FileName} ({FileSize} bytes)";
}
