﻿using Newtonsoft.Json;

namespace Studip.NET.Schema;

public interface IJsonApiEntity {
    string Id { get; }
    string Type { get; }
}

[JsonObject(MemberSerialization = MemberSerialization.OptIn)]
public class JsonApiEntity : IJsonApiEntity {
    [JsonProperty("id", Required = Required.Always)] public string Id { get; set; } = default!;
    [JsonProperty("type", Required = Required.Always)] public string Type { get; set; } = default!;
}