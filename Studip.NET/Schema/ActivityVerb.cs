﻿namespace Studip.NET.Schema;

public enum ActivityVerb {
    answered, attempted, attended, completed, created, deleted, edited, experienced, failed, imported, interacted, passed, shared, sent, voided
}