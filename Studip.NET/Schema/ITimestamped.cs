﻿namespace Studip.NET.Schema;

public interface ITimestamped {
    DateTime CreationDate { get; set; }
    DateTime ModificationDate { get; set; }
}