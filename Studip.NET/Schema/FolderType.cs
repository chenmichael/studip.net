﻿namespace Studip.NET.Schema;

public enum FolderType {
    RootFolder, StandardFolder, MaterialFolder, HomeworkFolder, TimedFolder
}