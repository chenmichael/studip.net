﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

/// <summary>
/// Activity Streams enthalten Objekte des Typs "activities". Diese geben enthalten eine textuelle Beschreibung der Aktivität, ein Datum, eine (etwas) detailiertere Beschreibung und die Art der Aktivität (das Verb). Aktivitäten beziehen sich auf einen Akteur (in der Regel ein Nutzer), einen Kontext, in dem sie stattfinden, und ein Objekt, auf das sie sich beziehen
/// </summary>
[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class Activity : JsonApiEntity {
    /// <summary>
    /// knappe Beschreibung der Aktivität: "Wer tut was mit wem/was wo?"
    /// </summary>
    [JsonProperty("title")] public string Title { get; set; } = default!;
    /// <summary>
    /// Datum der Aktivität
    /// </summary>
    [JsonProperty("mkdate")] public DateTime Date { get; set; }
    /// <summary>
    /// Art der Aktivität
    /// </summary>
    [JsonProperty("content")] public string Content { get; set; } = default!;
    /// <summary>
    /// Art der Aktivität
    /// </summary>
    [JsonProperty("verb")] public ActivityVerb Verb { get; set; } = default!;
    /// <summary>
    /// Typ der Aktivität
    /// </summary>
    [JsonProperty("activity-type")] public string ActivityType { get; set; } = default!;
    /// <summary>
    /// Wenn der Akteur der Aktivität ein Nutzer ist, wird mit dieser Relation auf ihn verwiesen.
    /// </summary>
    [JsonProperty("actor")] public Relationship<JsonApiEntity> Actor { get; set; } = default!;
    /// <summary>
    /// das Objekt, mit dem die Aktivität stattfindet; falls möglich wird hier auf eine Route in der JSON:API verwiesen
    /// </summary>
    [JsonProperty("object")] public Relationship<JsonApiEntity> Object { get; set; } = default!;
    /// <summary>
    /// der Kontext, in dem die Aktivität stattfindet; kann eine der folgenden sein: Veranstaltung, Einrichtung, Nutzer oder System.
    /// </summary>
    [JsonProperty("context")] public Relationship<JsonApiEntity> Context { get; set; } = default!;
    [JsonProperty("links")] public Links Links { get; set; } = default!;
    public override string ToString() => $"Activity";
}