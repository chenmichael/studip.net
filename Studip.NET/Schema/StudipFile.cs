﻿using System.Text;

namespace Studip.NET.Schema;

public abstract class StudipFile {
    public abstract byte[] Content { get; }
}

public class FileBytes : StudipFile {
    public FileBytes(byte[] content) => Content = content;
    public override byte[] Content { get; }
}

public class FileLink : StudipFile {
    public FileLink(Uri url) => Url = url;
    public Uri Url { get; }
    public override byte[] Content => GenerateUrlFile();
    private byte[] GenerateUrlFile() => Encoding.UTF8.GetBytes($"[InternetShortcut]\nURL={Url.AbsoluteUri}");
}