﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

/// <summary>
/// Ankündigungen bestehen aus ihrem Inhalt und einigen Meta-Daten. Die Dauer der Sichtbarkeit einer Anküdnigung wird durch ihre Attribute publication-start und end bestimmt (siehe Relationen).
/// </summary>
[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class News : JsonApiEntity, ITimestamped {
    /// <summary>
    /// Name einer news
    /// </summary>
    [JsonProperty("title")] public string Title { get; set; } = default!;
    /// <summary>
    /// Inhalt einer News
    /// </summary>
    [JsonProperty("content")] public string Content { get; set; } = default!;
    /// <summary>
    /// das Erstellungsdatum der Datei
    /// </summary>
    [JsonProperty("mkdate")] public DateTime CreationDate { get; set; }
    /// <summary>
    /// Datum der letzten Änderung
    /// </summary>
    [JsonProperty("chdate")] public DateTime ModificationDate { get; set; }
    /// <summary>
    /// Start der Sichtbarkeit für den Nutzerkreis einer News
    /// </summary>
    [JsonProperty("publication-start")] public DateTime PublicationStart { get; set; }
    /// <summary>
    /// Ende der Sichtbarkeit für den Nutzerkreis einer News
    /// </summary>
    [JsonProperty("publication-end")] public DateTime PublicationEnd { get; set; }
    /// <summary>
    /// Bestimmung, ob Kommentare erlaubt sind
    /// </summary>
    [JsonProperty("comments-allowed")] public bool CommentsAllowed { get; set; }
    /// <summary>
    /// Ersteller einer News
    /// </summary>
    [JsonProperty("author")] public Relationship<JsonApiEntity> Author { get; set; } = default!;
    /// <summary>
    /// global, institute, semester, course, users
    /// </summary>
    [JsonProperty("ranges")] public Relationship<JsonApiEntity> Ranges { get; set; } = default!;
    [JsonProperty("links")] public Links Links { get; set; } = default!;
    public override string ToString() => $"Activity";
}