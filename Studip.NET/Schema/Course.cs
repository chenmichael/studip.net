﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

/// <summary>
/// Kommentare werden in Stud.IP an eine Ankündigung angehangen, wenn der Ersteller der News die Erlaubnis vergeben hat.
/// </summary>
[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class Course : JsonApiEntity {
    /// <summary>
    /// ID des Kurses
    /// </summary>
    [JsonProperty("course-number")] public string? CourseNumber { get; set; }
    /// <summary>
    /// Beschreibung des Kurses
    /// </summary>
    [JsonProperty("description")] public string? Description { get; set; }
    /// <summary>
    /// Titel des Kurses
    /// </summary>
    [JsonProperty("title")] public string Title { get; set; } = default!;
    /// <summary>
    /// Untertitel des Kurses
    /// </summary>
    [JsonProperty("subtitle")] public string? Subtitle { get; set; }
    /// <summary>
    /// Art des Kurses
    /// </summary>
    [JsonProperty("course-type")] public int CourseType { get; set; }
    /// <summary>
    /// Die zugewiesene Institution
    /// </summary>
    [JsonProperty("institute")] public Relationship<JsonApiEntity> Institute { get; set; } = default!;
    /// <summary>
    /// Anfangs-Semester der Veranstaltung
    /// </summary>
    [JsonProperty("start-semester")] public Relationship<JsonApiEntity> StartSemester { get; set; } = default!;
    /// <summary>
    /// End-Semester der Veranstaltung
    /// </summary>
    [JsonProperty("end-semester")] public Relationship<JsonApiEntity> EndSemester { get; set; } = default!;
    /// <summary>
    /// Referenz auf Files innerhalb der Veranstaltung
    /// </summary>
    [JsonProperty("files")] public Relationship<JsonApiEntity> Files { get; set; } = default!;
    /// <summary>
    /// Referenz auf Dokumente innerhalb der Veranstaltung
    /// </summary>
    [JsonProperty("documents")] public Relationship<JsonApiEntity> Documents { get; set; } = default!;
    /// <summary>
    /// Referenz auf Dokumente innerhalb der Veranstaltung
    /// </summary>
    [JsonProperty("folders")] public Relationship<JsonApiEntity> DocumentFolders { get; set; } = default!;
}
