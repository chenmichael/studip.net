﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

/// <summary>
/// Kommentare werden in Stud.IP an eine Ankündigung angehangen, wenn der Ersteller der News die Erlaubnis vergeben hat.
/// </summary>
[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class Comment : JsonApiEntity, ITimestamped {
    /// <summary>
    /// eine optionale Beschreibung der Datei
    /// </summary>
    [JsonProperty("content")] public string Content { get; set; } = default!;
    /// <summary>
    /// das Erstellungsdatum der Datei
    /// </summary>
    [JsonProperty("mkdate")] public DateTime CreationDate { get; set; }
    /// <summary>
    /// das Datum der letzten Änderung der Metadaten ('name', 'description', …) der Datei
    /// </summary>
    [JsonProperty("chdate")] public DateTime ModificationDate { get; set; }
    /// <summary>
    /// die Veranstaltung, die Einrichtung oder der Nutzer, in dessen Dateibereich diese Datei liegt
    /// </summary>
    [JsonProperty("author")] public Relationship<JsonApiEntity> Author { get; set; } = default!;
    /// <summary>
    /// die Veranstaltung, die Einrichtung oder der Nutzer, in dessen Dateibereich diese Datei liegt
    /// </summary>
    [JsonProperty("news")] public Relationship<JsonApiEntity> News { get; set; } = default!;
    /// <summary>
    /// die Lizenz, unter der diese Datei verfügbar gemacht wird
    /// </summary>
    [JsonProperty("terms-of-use")] public Relationship<JsonApiEntity> License { get; set; } = default!;
}
