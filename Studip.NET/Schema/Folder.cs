﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

/// <summary>
/// Ressourcen vom Typ "folders" sind im herkömmlichen Sinne Ordner und können weitere "folders" oder Ressourcen vom Typ "file-refs" enthalten.
/// Es gibt verschiedene Arten von "folders". In Stud.IP werden aber vorrangig "StandardFolders" verwendet.Für diese sind alle Operationen möglich. Für andere Arten entscheiden die Implementierungen jeweils selbst, ob die Operation möglich ist.
/// </summary>
[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class Folder : JsonApiEntity, IFolderContained {
    /// <summary>
    /// die Art des Ordners
    /// </summary>
    [JsonProperty("folder-type")] public FolderType FolderType { get; set; } = default!;
    /// <summary>
    /// der Name des Ordners
    /// </summary>
    [JsonProperty("name")] public string Name { get; set; } = default!;
    /// <summary>
    /// die Beschreibung des Ordners
    /// </summary>
    [JsonProperty("description")] public string? Description { get; set; }
    /// <summary>
    /// das Erstellungsdatum des Ordners
    /// </summary>
    [JsonProperty("mkdate")] public DateTime CreationDate { get; set; }
    /// <summary>
    /// Datum der letzten Änderung
    /// </summary>
    [JsonProperty("chdate")] public DateTime ModificationDate { get; set; }
    [JsonProperty("is-readable")] public bool IsReadable { get; set; }
    [JsonProperty("is-downloadable")] public bool IsDownloadable { get; set; }
    [JsonProperty("is-editable")] public bool IsEditable { get; set; }
    [JsonProperty("is-writable")] public bool IsWritable { get; set; }
    [JsonProperty("is-subfolder-allowed")] public bool IsSubfolderAllowed { get; set; }
    [JsonProperty("owner")] public Relationship<JsonApiEntity> Owner { get; set; } = default!;
    [JsonProperty("parent")] public Relationship<JsonApiEntity>? Parent { get; set; } = default!;
    [JsonProperty("file-refs")] public Relationship<JsonApiEntity> FileRefs { get; set; } = default!;
    [JsonProperty("folders")] public Relationship<JsonApiEntity> Folders { get; set; } = default!;
    [JsonProperty("range")] public Relationship<JsonApiEntity> Range { get; set; } = default!;
}