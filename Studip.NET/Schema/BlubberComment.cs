﻿using JsonApiSerializer.JsonApi;
using Newtonsoft.Json;
using System.Diagnostics;

namespace Studip.NET.Schema;

[DebuggerDisplay($"{{{nameof(ToString)}(),nq}}")]
public class BlubberComment : JsonApiEntity, ITimestamped {
    [JsonProperty("content")] public string Content { get; set; } = default!;
    [JsonProperty("content-html")] public string ContentHtml { get; set; } = default!;
    [JsonProperty("mkdate")] public DateTime CreationDate { get; set; }
    [JsonProperty("chdate")] public DateTime ModificationDate { get; set; }
    [JsonProperty("author")] public Relationship<JsonApiEntity> Author { get; set; } = default!;
    [JsonProperty("thread")] public Relationship<JsonApiEntity> Thread { get; set; } = default!;
}
