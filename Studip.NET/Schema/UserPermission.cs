﻿namespace Studip.NET.Schema;

public enum UserPermission {
    root, admin, dozent, tutor, autor
}