﻿using JsonApiSerializer.JsonApi;

namespace Studip.NET.Schema;

public interface IFolderContained {
    Relationship<JsonApiEntity>? Parent { get; set; }
}