﻿using RestSharp;
using Studip.NET.Schema;

namespace Studip.NET;

public interface IRequestHandler {
    /// <summary>
    /// Gets an entity of a given type <typeparamref name="T"/> from the JSON API endpoint on the server.
    /// </summary>
    /// <typeparam name="T">Type of the entity to expect from the server API.</typeparam>
    /// <param name="resource">Path of the resource on the server.</param>
    /// <param name="ct">CancellationToken to cancel the request.</param>
    /// <returns>The parsed entity received from the server.</returns>
    Task<T> GetJsonAsync<T>(string resource, CancellationToken ct);
    IAsyncEnumerable<T> PaginationAsync<T>(string resource, CancellationToken ct);
    HttpClient GetDomainBaseClient();
    RestClient Client { get; }
}