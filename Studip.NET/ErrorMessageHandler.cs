﻿using System.Net;

namespace Studip.NET;

public class ErrorMessageHandler : DelegatingHandler {
    public ErrorMessageHandler(HttpMessageHandler innerHandler) : base(innerHandler) { }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {
        HttpResponseMessage response;
        response = await base.SendAsync(request, cancellationToken);
        return response.StatusCode switch {
            HttpStatusCode.NotFound => throw new InvalidProgramException($"Oops, developer misconfigured a route: {request.RequestUri}!"),
            HttpStatusCode.Unauthorized => throw new InvalidProgramException($"Oops, developer forgot to authorize to: {request.RequestUri}!"),
            _ => response,
        };
    }
}