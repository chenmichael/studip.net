﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class BlubberThreadApiEndpoint : EntityEndpointBase<BlubberThread> {
    public BlubberThreadApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }

    public IAsyncEnumerable<BlubberComment> GetCommentsAsync(CancellationToken ct) => rh.PaginationAsync<BlubberComment>($"{BaseUrl}/comments", ct);
    public IAsyncEnumerable<JsonApiEntity> GetRelationshipMentionsAsync(CancellationToken ct) => rh.PaginationAsync<JsonApiEntity>($"{BaseUrl}/relationships/mentions", ct);
}