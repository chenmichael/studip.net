﻿namespace Studip.NET.Endpoints;

public abstract class RequestHandlerBase {
    protected RequestHandlerBase(IRequestHandler rh) => this.rh = rh;

    protected readonly IRequestHandler rh;
    protected abstract string BaseUrl { get; }
}

