﻿namespace Studip.NET.Endpoints;

public class WikiPagesApiEndpoint : EndpointRouteBase<WikiPageApiEndpoint> {
    public WikiPagesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/wiki-pages") { }

    public override WikiPageApiEndpoint this[string id] => new(rh, BaseUrl, id);
}