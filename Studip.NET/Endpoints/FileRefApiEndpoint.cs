﻿using Studip.NET.Schema;
using System.Net;

namespace Studip.NET.Endpoints;

public class FileRefApiEndpoint : EntityEndpointBase<FileRef> {
    public FileRefApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }

    public async Task<StudipFile> GetContentAsync(CancellationToken ct) {
        const string filename = "file.pdf";
        using var httpClient = rh.GetDomainBaseClient();
        var response = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, $"/studip/sendfile.php?type=0&file_id={Id}&file_name={filename}"), ct);
        switch (response.StatusCode) {
        case HttpStatusCode.OK:
            return new FileBytes(await response.Content.ReadAsByteArrayAsync(ct));
        default:
            if (response.Headers.Location is Uri location) return new FileLink(location);
            throw new InvalidProgramException($"Unknown file download status code: {response.StatusCode}!");
        }
    }
    public IAsyncEnumerable<FeedbackElement> GetFeedbackElementsAsync(CancellationToken ct) => rh.PaginationAsync<FeedbackElement>($"{BaseUrl}/feedback-elements", ct);
    public Task<JsonApiEntity> GetRelationshipTermsOfUseAsync(CancellationToken ct) => rh.GetJsonAsync<JsonApiEntity>($"{BaseUrl}/relationships/terms-of-use", ct);
}