﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public interface IEntityEndpoint<T> where T : notnull, IJsonApiEntity {
    Task<T> GetInfoAsync(CancellationToken ct);
}