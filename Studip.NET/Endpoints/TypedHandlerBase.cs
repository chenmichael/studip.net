﻿namespace Studip.NET.Endpoints;

public abstract class TypedHandlerBase<TEntity> : RequestHandlerBase {
    protected TypedHandlerBase(IRequestHandler rh) : base(rh) { }

    // TODO: get type name from entity schema attribute instead of manual overrides

    public abstract string Type { get; }
    protected override string BaseUrl => $"{StudipClient.RequestBase}/{Type}";
}

