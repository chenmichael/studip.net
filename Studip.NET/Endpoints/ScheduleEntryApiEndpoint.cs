﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class ScheduleEntryApiEndpoint : EntityEndpointBase<ScheduleEntry> {
    public ScheduleEntryApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}