﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class TermOfUseApiEndpoint : EntityEndpointBase<TermsOfUse> {
    public TermOfUseApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}