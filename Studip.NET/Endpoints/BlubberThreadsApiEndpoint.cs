﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class BlubberThreadsApiEndpoint : EndpointRouteBase<BlubberThreadApiEndpoint> {
    public BlubberThreadsApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/blubber-threads") { }

    public override BlubberThreadApiEndpoint this[string id] => new(rh, BaseUrl, id);
    public IAsyncEnumerable<BlubberThread> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<BlubberThread>(BaseUrl, ct);
}