﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class FolderApiEndpoint : EntityEndpointBase<Folder> {
    public FolderApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }

    public IAsyncEnumerable<FeedbackElement> GetFeedbackElementsAsync(CancellationToken ct) => rh.PaginationAsync<FeedbackElement>($"{BaseUrl}/feedback-elements", ct);
    public IAsyncEnumerable<FileRef> GetFileRefsAsync(CancellationToken ct) => rh.PaginationAsync<FileRef>($"{BaseUrl}/file-refs", ct);
    public IAsyncEnumerable<Folder> GetFoldersAsync(CancellationToken ct) => rh.PaginationAsync<Folder>($"{BaseUrl}/folders", ct);
}