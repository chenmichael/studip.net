﻿namespace Studip.NET.Endpoints;

public class MessagesApiEndpoint : EndpointRouteBase<MessageApiEndpoint> {
    public MessagesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/messages") { }

    public override MessageApiEndpoint this[string id] => new(rh, BaseUrl, id);
}