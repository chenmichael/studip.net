﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class WikiPageApiEndpoint : EntityEndpointBase<WikiPage> {
    public WikiPageApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}