﻿namespace Studip.NET.Endpoints;

public class FileRefsApiEndpoint : EndpointRouteBase<FileRefApiEndpoint> {
    public FileRefsApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/file-refs") { }

    public override FileRefApiEndpoint this[string id] => new(rh, BaseUrl, id);
}