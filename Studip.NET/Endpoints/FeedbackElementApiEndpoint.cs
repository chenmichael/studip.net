﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class FeedbackElementApiEndpoint : EntityEndpointBase<FeedbackElement> {
    public FeedbackElementApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }

    public IAsyncEnumerable<FeedbackEntry> GetFeedbackEntriesAsync(CancellationToken ct) => rh.PaginationAsync<FeedbackEntry>($"{BaseUrl}/entries", ct);
}