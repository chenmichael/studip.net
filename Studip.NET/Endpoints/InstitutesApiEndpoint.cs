﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class InstitutesApiEndpoint : EndpointRouteBase<InstituteApiEndpoint> {
    public InstitutesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/institutes") { }

    public override InstituteApiEndpoint this[string id] => new(rh, BaseUrl, id);
    public IAsyncEnumerable<Institute> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<Institute>(BaseUrl, ct);
}