﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class FeedbackEntryApiEndpoint : EntityEndpointBase<FeedbackEntry> {
    public FeedbackEntryApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}