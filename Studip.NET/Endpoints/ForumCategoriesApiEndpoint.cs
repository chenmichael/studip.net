﻿namespace Studip.NET.Endpoints;

public class ForumCategoriesApiEndpoint : EndpointRouteBase<ForumCategoryApiEndpoint> {
    public ForumCategoriesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/forum-categories") { }

    public override ForumCategoryApiEndpoint this[string id] => new(rh, BaseUrl, id);
}