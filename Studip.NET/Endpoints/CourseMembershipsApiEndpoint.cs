﻿namespace Studip.NET.Endpoints;

public class CourseMembershipsApiEndpoint : EndpointRouteBase<CourseMembershipApiEndpoint> {
    public CourseMembershipsApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/course-memberships") { }

    public override CourseMembershipApiEndpoint this[string id] => new(rh, BaseUrl, id);
}