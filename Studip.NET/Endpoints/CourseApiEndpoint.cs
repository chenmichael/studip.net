﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class CourseApiEndpoint : EntityEndpointBase<Course> {
    public CourseApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }

    public IAsyncEnumerable<BlubberThread> GetBlubberThreadsAsync(CancellationToken ct) => rh.PaginationAsync<BlubberThread>($"{BaseUrl}/blubber-threads", ct);
    public IAsyncEnumerable<Event> GetEventsAsync(CancellationToken ct) => rh.PaginationAsync<Event>($"{BaseUrl}/events", ct);
    public IAsyncEnumerable<FeedbackElement> GetFeedbackElementsAsync(CancellationToken ct) => rh.PaginationAsync<FeedbackElement>($"{BaseUrl}/feedback-elements", ct);
    public IAsyncEnumerable<FileRef> GetFileRefsAsync(CancellationToken ct) => rh.PaginationAsync<FileRef>($"{BaseUrl}/file-refs", ct);
    public IAsyncEnumerable<Folder> GetFoldersAsync(CancellationToken ct) => rh.PaginationAsync<Folder>($"{BaseUrl}/folders", ct);
    public IAsyncEnumerable<ForumCategory> GetForumCategoriesAsync(CancellationToken ct) => rh.PaginationAsync<ForumCategory>($"{BaseUrl}/forum-categories", ct);
    public IAsyncEnumerable<CourseMembership> GetCourseMembershipsAsync(CancellationToken ct) => rh.PaginationAsync<CourseMembership>($"{BaseUrl}/memberships", ct);
    public IAsyncEnumerable<News> GetNewsAsync(CancellationToken ct) => rh.PaginationAsync<News>($"{BaseUrl}/news", ct);
    public IAsyncEnumerable<JsonApiEntity> GetRelationshipMemberships(CancellationToken ct) => rh.PaginationAsync<JsonApiEntity>($"{BaseUrl}/relationships/memberships", ct);
    public IAsyncEnumerable<WikiPage> GetWikiPagesAsync(CancellationToken ct) => rh.PaginationAsync<WikiPage>($"{BaseUrl}/wiki-pages", ct);
}