﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class BlubberCommentsApiEndpoint : EndpointRouteBase<BlubberCommentApiEndpoint> {
    public BlubberCommentsApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/blubber-comments") { }

    public override BlubberCommentApiEndpoint this[string id] => new(rh, BaseUrl, id);
    public IAsyncEnumerable<BlubberComment> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<BlubberComment>(BaseUrl, ct);
}