﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class SemesterApiEndpoint : EntityEndpointBase<Semester> {
    public SemesterApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}