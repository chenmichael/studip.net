﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class MessageApiEndpoint : EntityEndpointBase<Message> {
    public MessageApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}