﻿namespace Studip.NET.Endpoints;

[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
public sealed class SchemaNameAttribute : Attribute {
    public SchemaNameAttribute(string type) => Type = type;

    public string Type { get; }
}

