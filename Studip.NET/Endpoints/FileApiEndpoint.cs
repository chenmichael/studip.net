﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class FileApiEndpoint : EntityEndpointBase<ActualFile> {
    public FileApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }

    public IAsyncEnumerable<FileRef> GetFileRefsAsync(CancellationToken ct) => rh.PaginationAsync<FileRef>($"{BaseUrl}/file-refs", ct);
    public IAsyncEnumerable<JsonApiEntity> GetRelationshipFileRefsAsync(CancellationToken ct) => rh.PaginationAsync<JsonApiEntity>($"{BaseUrl}/relationships/file-refs", ct);
}