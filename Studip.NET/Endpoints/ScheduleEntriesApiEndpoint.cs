﻿namespace Studip.NET.Endpoints;

public class ScheduleEntriesApiEndpoint : EndpointRouteBase<ScheduleEntryApiEndpoint> {
    public ScheduleEntriesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/schedule-entries") { }

    public override ScheduleEntryApiEndpoint this[string id] => new(rh, BaseUrl, id);
}