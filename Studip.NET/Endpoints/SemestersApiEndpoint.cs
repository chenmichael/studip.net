﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class SemestersApiEndpoint : EndpointRouteBase<SemesterApiEndpoint> {
    public SemestersApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/semesters") { }

    public override SemesterApiEndpoint this[string id] => new(rh, BaseUrl, id);
    public IAsyncEnumerable<Semester> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<Semester>(BaseUrl, ct);
}