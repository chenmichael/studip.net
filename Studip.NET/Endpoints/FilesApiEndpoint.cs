﻿namespace Studip.NET.Endpoints;

public class FilesApiEndpoint : EndpointRouteBase<FileApiEndpoint> {
    public FilesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/files") { }

    public override FileApiEndpoint this[string id] => new(rh, BaseUrl, id);
}