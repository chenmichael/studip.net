﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class InstituteMembershipApiEndpoint : EntityEndpointBase<InstituteMembership> {
    public InstituteMembershipApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}