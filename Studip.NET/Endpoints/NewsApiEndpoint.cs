﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class NewsApiEndpoint : EndpointRouteBase<NewsIdApiEndpoint> {
    public NewsApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/news") { }

    public override NewsIdApiEndpoint this[string id] => new(rh, BaseUrl, id);
    public IAsyncEnumerable<News> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<News>(BaseUrl, ct);
}