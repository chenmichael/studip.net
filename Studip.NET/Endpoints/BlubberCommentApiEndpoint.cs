﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class BlubberCommentApiEndpoint : EntityEndpointBase<BlubberComment> {
    public BlubberCommentApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}