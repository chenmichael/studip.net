﻿namespace Studip.NET.Endpoints;

public class SeminarCycleDatesApiEndpoint : EndpointRouteBase<SeminarCycleDateApiEndpoint> {
    public SeminarCycleDatesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/seminar-cycle-dates") { }

    public override SeminarCycleDateApiEndpoint this[string id] => new(rh, BaseUrl, id);
}