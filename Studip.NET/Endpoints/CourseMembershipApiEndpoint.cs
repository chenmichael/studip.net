﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class CourseMembershipApiEndpoint : EntityEndpointBase<CourseMembership> {
    public CourseMembershipApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}