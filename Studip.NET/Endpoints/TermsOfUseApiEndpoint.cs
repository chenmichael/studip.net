﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class TermsOfUseApiEndpoint : EndpointRouteBase<TermOfUseApiEndpoint> {
    public TermsOfUseApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/terms-of-use") { }

    public override TermOfUseApiEndpoint this[string id] => new(rh, BaseUrl, id);
    public IAsyncEnumerable<TermsOfUse> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<TermsOfUse>(BaseUrl, ct);
}