﻿using RestSharp;
using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class UserEndpoint : EntityHandlerBase<User> {
    public UserEndpoint(IRequestHandler rh, string id) : base(rh, id) { }

    public override string Type => "users"; // Somehow Reflection using the SchemaNameAttribute fails the test system... so this is my life for now

    public IAsyncEnumerable<Activity> GetActivitiesAsync(CancellationToken ct) => rh.PaginationAsync<Activity>($"{BaseUrl}/activitystream", ct);
    public IAsyncEnumerable<BlubberThread> GetBlubberThreadsAsync(CancellationToken ct) => rh.PaginationAsync<BlubberThread>($"{BaseUrl}/blubber-threads", ct);
    public IAsyncEnumerable<Contact> GetContactsAsync(CancellationToken ct) => rh.PaginationAsync<Contact>($"{BaseUrl}/contacts", ct);
    public IAsyncEnumerable<CourseMembership> GetCourseMembershipsAsync(CancellationToken ct) => rh.PaginationAsync<CourseMembership>($"{BaseUrl}/memberships", ct);
    public IAsyncEnumerable<Course> GetCoursesAsync(CancellationToken ct) => rh.PaginationAsync<Course>($"{BaseUrl}/courses", ct);
    public IAsyncEnumerable<Event> GetEventsAsync(CancellationToken ct) => rh.PaginationAsync<Event>($"{BaseUrl}/events", ct);
    public async Task<string> GetCalendarAsync(CancellationToken ct)
        => (await rh.Client.GetAsync(new($"{BaseUrl}/events.ics"), ct))
        .Content ?? throw new InvalidProgramException("Server response had no content!");
    public IAsyncEnumerable<FileRef> GetFileRefsAsync(CancellationToken ct) => rh.PaginationAsync<FileRef>($"{BaseUrl}/file-refs", ct);
    public IAsyncEnumerable<Folder> GetFoldersAsync(CancellationToken ct) => rh.PaginationAsync<Folder>($"{BaseUrl}/folders", ct);
    public IAsyncEnumerable<Message> GetInboxAsync(CancellationToken ct) => rh.PaginationAsync<Message>($"{BaseUrl}/inbox", ct);
    public IAsyncEnumerable<InstituteMembership> GetInstituteMembershipsAsync(string id, CancellationToken ct) => rh.PaginationAsync<InstituteMembership>($"{BaseUrl}/institute-memberships", ct);
    public IAsyncEnumerable<News> GetNewsAsync(CancellationToken ct) => rh.PaginationAsync<News>($"{BaseUrl}/news", ct);
    public IAsyncEnumerable<Message> GetOutboxAsync(CancellationToken ct) => rh.PaginationAsync<Message>($"{BaseUrl}/outbox", ct);
    public IAsyncEnumerable<JsonApiEntity> GetRelationshipContactsAsync(CancellationToken ct) => rh.PaginationAsync<JsonApiEntity>($"{BaseUrl}/relationships/contacts", ct);
    public IAsyncEnumerable<SeminarCycleDate> GetScheduleAsync(CancellationToken ct) => rh.PaginationAsync<SeminarCycleDate>($"{BaseUrl}/wiki-pages", ct);
}

