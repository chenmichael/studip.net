﻿namespace Studip.NET.Endpoints;

public interface IEndpointRoute<TEndpoint> {
    TEndpoint this[string id] { get; }
}