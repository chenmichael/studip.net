﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class CoursesApiEndpoint : EndpointRouteBase<CourseApiEndpoint> {
    public CoursesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/courses") { }

    public override CourseApiEndpoint this[string id] => new(rh, BaseUrl, id);
    public IAsyncEnumerable<Course> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<Course>(BaseUrl, ct);
}