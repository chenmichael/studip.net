﻿namespace Studip.NET.Endpoints;

public abstract class EndpointRouteBase<TEndpoint> : IEndpointRoute<TEndpoint> {
    protected EndpointRouteBase(IRequestHandler requestHandler, string baseUrl) {
        rh = requestHandler;
        BaseUrl = baseUrl;
    }

    protected readonly IRequestHandler rh;
    public string BaseUrl { get; }

    public abstract TEndpoint this[string id] { get; }
}