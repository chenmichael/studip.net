﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class FeedbackElementsApiEndpoint : EndpointRouteBase<FeedbackElementApiEndpoint> {
    public FeedbackElementsApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/feedback-elements") { }

    public override FeedbackElementApiEndpoint this[string id] => new(rh, BaseUrl, id);
    public IAsyncEnumerable<FeedbackElement> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<FeedbackElement>(BaseUrl, ct);
}