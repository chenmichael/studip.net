﻿namespace Studip.NET.Endpoints;

public class ForumEntriesApiEndpoint : EndpointRouteBase<ForumEntryApiEndpoint> {
    public ForumEntriesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/forum-entries") { }

    public override ForumEntryApiEndpoint this[string id] => new(rh, BaseUrl, id);
}