﻿namespace Studip.NET.Endpoints;

public class InstituteMembershipsApiEndpoint : EndpointRouteBase<InstituteMembershipApiEndpoint> {
    public InstituteMembershipsApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/institute-memberships") { }

    public override InstituteMembershipApiEndpoint this[string id] => new(rh, BaseUrl, id);
}