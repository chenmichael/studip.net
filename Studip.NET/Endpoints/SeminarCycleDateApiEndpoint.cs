﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class SeminarCycleDateApiEndpoint : EntityEndpointBase<SeminarCycleDate> {
    public SeminarCycleDateApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }
}