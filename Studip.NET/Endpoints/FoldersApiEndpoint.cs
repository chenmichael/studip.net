﻿namespace Studip.NET.Endpoints;

public class FoldersApiEndpoint : EndpointRouteBase<FolderApiEndpoint> {
    public FoldersApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/folders") { }

    public override FolderApiEndpoint this[string id] => new(rh, BaseUrl, id);
}