﻿namespace Studip.NET.Endpoints;

public class FeedbackEntriesApiEndpoint : EndpointRouteBase<FeedbackEntryApiEndpoint> {
    public FeedbackEntriesApiEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/feedback-entries") { }

    public override FeedbackEntryApiEndpoint this[string id] => new(rh, BaseUrl, id);
}