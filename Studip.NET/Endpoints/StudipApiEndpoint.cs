﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class StudipApiEndpoint {
    public const string BaseUrl = $"{StudipClient.RequestBase}/studip";
    private readonly IRequestHandler rh;

    public StudipApiEndpoint(IRequestHandler requestHandler) => rh = requestHandler;

    public ValueTask<StudipProperties[]> GetPropertiesAsync(CancellationToken ct) => rh.PaginationAsync<StudipProperties>($"{BaseUrl}/properties", ct).ToArrayAsync(ct);
    public IAsyncEnumerable<News> GetNewsAsync(CancellationToken ct) => rh.PaginationAsync<News>($"{BaseUrl}/news", ct);
    public IAsyncEnumerable<BlubberThread> GetBlubberThreadsAsync(CancellationToken ct) => rh.PaginationAsync<BlubberThread>($"{BaseUrl}/blubber-threads", ct);
}