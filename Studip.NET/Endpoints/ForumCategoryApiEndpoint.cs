﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class ForumCategoryApiEndpoint : EntityEndpointBase<ForumCategory> {
    public ForumCategoryApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }

    public IAsyncEnumerable<ForumEntry> GetForumEntriesAsync(CancellationToken ct) => rh.PaginationAsync<ForumEntry>($"{BaseUrl}/entries", ct);
}