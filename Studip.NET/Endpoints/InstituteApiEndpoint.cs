﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class InstituteApiEndpoint : EntityEndpointBase<Institute> {
    public InstituteApiEndpoint(IRequestHandler rh, string baseUrl, string id) : base(rh, baseUrl, id) { }

    public IAsyncEnumerable<BlubberThread> GetBlubberThreadsAsync(CancellationToken ct) => rh.PaginationAsync<BlubberThread>($"{BaseUrl}/blubber-threads", ct);
    public IAsyncEnumerable<FileRef> GetFileRefsAsync(CancellationToken ct) => rh.PaginationAsync<FileRef>($"{BaseUrl}/file-refs", ct);
    public IAsyncEnumerable<Folder> GetFoldersAsync(CancellationToken ct) => rh.PaginationAsync<Folder>($"{BaseUrl}/folders", ct);
}