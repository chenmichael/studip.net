﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public class UsersEndpoint : EndpointRouteBase<UserEndpoint> {
    public UsersEndpoint(IRequestHandler requestHandler) : base(requestHandler, $"{StudipClient.RequestBase}/users") { }

    public override UserEndpoint this[string id] => new(rh, id);

    public IAsyncEnumerable<User> GetAllAsync(CancellationToken ct) => rh.PaginationAsync<User>(BaseUrl, ct);
    public async Task<UserEndpoint> GetSelfAsync(CancellationToken ct) => this[(await rh.GetJsonAsync<JsonApiEntity>($"{BaseUrl}/me", ct)).Id];
}