﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public abstract class EntityEndpointBase<T> : IEntityEndpoint<T> where T : notnull, IJsonApiEntity {
    protected readonly IRequestHandler rh;

    public EntityEndpointBase(IRequestHandler rh, string baseUrl, string id) {
        this.rh = rh;
        BaseUrl = $"{baseUrl}/{id}";
        Id = id;
    }

    public string BaseUrl { get; }
    public string Id { get; }

    public Task<T> GetInfoAsync(CancellationToken ct) => rh.GetJsonAsync<T>(BaseUrl, ct);
}