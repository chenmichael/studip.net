﻿using Studip.NET.Schema;

namespace Studip.NET.Endpoints;

public abstract class EntityHandlerBase<TEntity> : TypedHandlerBase<TEntity>, IJsonApiEntity where TEntity : notnull, IJsonApiEntity {
    protected EntityHandlerBase(IRequestHandler rh, string id) : base(rh) => Id = id;

    public string Id { get; }
    protected override string BaseUrl => $"{base.BaseUrl}/{Id}";
    public TEntity? Attributes { get; private set; }

    public virtual async Task<TEntity> FetchAsync(CancellationToken ct) => Attributes = await rh.GetJsonAsync<TEntity>(BaseUrl, ct);
}

