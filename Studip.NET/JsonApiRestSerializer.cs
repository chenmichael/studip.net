﻿using JsonApiSerializer;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Serializers;

namespace Studip.NET;

public class JsonApiRestSerializer : IRestSerializer, ISerializer, IDeserializer {
    public const string CONTENT_TYPE = "application/vnd.api+json";
    public ISerializer Serializer => this;
    public IDeserializer Deserializer => this;
    public string[] AcceptedContentTypes { get; } = new[] { CONTENT_TYPE };
    public SupportsContentType SupportsContentType => type => CONTENT_TYPE == type;
    public DataFormat DataFormat => DataFormat.Json;
    public string ContentType { get; set; } = CONTENT_TYPE;
    public T? Deserialize<T>(RestResponse response) => JsonConvert.DeserializeObject<T>(response.Content ?? throw new Exception("Response is null!"), new JsonApiSerializerSettings());
    public string? Serialize(Parameter parameter) => Serialize(parameter.Value ?? throw new Exception("Response is null!"));
    public string? Serialize(object obj) => JsonConvert.SerializeObject(obj, new JsonApiSerializerSettings());
}