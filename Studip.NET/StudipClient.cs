﻿using JsonApiSerializer.JsonApi;
using RestSharp;
using Studip.NET.Endpoints;
using Studip.NET.Schema;
using System.Runtime.CompilerServices;

namespace Studip.NET;

public class StudipClient : IRequestHandler {
    public const string RequestBase = "/studip/jsonapi.php/v1";
    public const string SessionCookieName = "Seminar_Session";

    public StudipClient(StudipClientOptions options) {
        Client = new RestClient(new RestClientOptions(options.RootUrl) {
            ThrowOnAnyError = true,
            ThrowOnDeserializationError = true,
            FollowRedirects = false,
            ConfigureMessageHandler = WrapMessageHandler(options)
        }) {
            AcceptedContentTypes = new[] { JsonApiRestSerializer.CONTENT_TYPE }
        };
        Client.AddCookie(SessionCookieName, options.SessionCookie, "/studip/", options.Domain);
        Client.UseSerializer<JsonApiRestSerializer>();
        Options = options;
        BlubberComments = new(this);
        BlubberThreads = new(this);
        CourseMemberships = new(this);
        Courses = new(this);
        FeedbackElements = new(this);
        FeedbackEntries = new(this);
        FileRefs = new(this);
        Files = new(this);
        Folders = new(this);
        ForumCategories = new(this);
        ForumEntries = new(this);
        InstituteMemberships = new(this);
        Institutes = new(this);
        Messages = new(this);
        News = new(this);
        ScheduleEntries = new(this);
        Semesters = new(this);
        Studip = new(this);
        SeminarCycleDates = new(this);
        TermsOfUse = new(this);
        Users = new(this);
        WikiPages = new(this);
    }

    private Func<HttpMessageHandler, HttpMessageHandler> WrapMessageHandler(StudipClientOptions options) {
        return i => {
            if (options.ConfigureMessageHandler is Func<HttpMessageHandler, HttpMessageHandler> configureHandler) i = configureHandler(i);
            return new ErrorMessageHandler(i);
        };
    }

    public StudipClientOptions Options { get; }
    public RestClient Client { get; }

    private static T NonNull<T>(T? res) => res is null ? throw new Exception("Deserialized response was null!") : res;

    public async Task<T> GetJsonAsync<T>(string resource, CancellationToken ct)
        => NonNull(await Client.GetJsonAsync<T>(resource, ct));

    public async IAsyncEnumerable<T> PaginationAsync<T>(string resource, [EnumeratorCancellation] CancellationToken ct) {
        while (true) {
            var page = await GetJsonAsync<DocumentRoot<List<T>>>(resource, ct);
            foreach (var item in page.Data) yield return item;
            if (page.Links is Links links && links.TryGetValue("next", out var nextPage)) resource = nextPage.Href;
            else break;
        }
    }

    #region Endpoints
    public BlubberCommentsApiEndpoint BlubberComments { get; }
    public BlubberThreadsApiEndpoint BlubberThreads { get; }
    public CourseMembershipsApiEndpoint CourseMemberships { get; }
    public CoursesApiEndpoint Courses { get; }
    public FeedbackElementsApiEndpoint FeedbackElements { get; }
    public FeedbackEntriesApiEndpoint FeedbackEntries { get; }
    public FileRefsApiEndpoint FileRefs { get; }
    public FilesApiEndpoint Files { get; }
    public FoldersApiEndpoint Folders { get; }
    public ForumCategoriesApiEndpoint ForumCategories { get; }
    public ForumEntriesApiEndpoint ForumEntries { get; }
    public InstituteMembershipsApiEndpoint InstituteMemberships { get; }
    public InstitutesApiEndpoint Institutes { get; }
    public MessagesApiEndpoint Messages { get; }
    public NewsApiEndpoint News { get; }
    public ScheduleEntriesApiEndpoint ScheduleEntries { get; }
    public SemestersApiEndpoint Semesters { get; }
    public StudipApiEndpoint Studip { get; }
    public SeminarCycleDatesApiEndpoint SeminarCycleDates { get; }
    public TermsOfUseApiEndpoint TermsOfUse { get; }
    public UsersEndpoint Users { get; }
    public WikiPagesApiEndpoint WikiPages { get; }
    #endregion

    public ValueTask<ApiRoute[]> GetAPIRoutesAsync(CancellationToken ct) => PaginationAsync<ApiRoute>($"{RequestBase}/discovery", ct).ToArrayAsync(ct);

    public HttpClient GetDomainBaseClient()
        => new(new HttpClientHandler() {
            CookieContainer = Client.CookieContainer,
            AllowAutoRedirect = false
        }, true) { BaseAddress = new(Options.RootUrl) };
}