using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Studip.NET.Schema;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Studip.NET.Tests;
[TestClass]
public class StudipTests {
    private const string Wise21Id = "6e3e1a5506d82fcbca75a2c73e90a126";
    private static readonly Credentials UserCredentials = JsonConvert.DeserializeObject<Credentials>(File.ReadAllText("credentials.json"))
        ?? throw new InvalidProgramException("Expected non-null user credentials!");
    public StudipClient Client { get => _client ?? throw new InvalidProgramException("Client uninitialized!"); private set => _client = value; }
    private StudipClient? _client;

    [TestInitialize]
    public async Task InitializeClient() {
        var sessionCookie = await StudipClientOptions.GetSessionCookie(UserCredentials.UserName, UserCredentials.Password);
        Client = new StudipClient(new(sessionCookie.Value) {
            ConfigureMessageHandler = (handler) => new DebuggingMessageHandler(handler)
        });
    }

    [TestMethod]
    public async Task TestGetSemester() {
        var wise21 = await Client.Semesters[Wise21Id].GetInfoAsync(default);
        Assert.IsNotNull(wise21);
        Assert.AreEqual("WiSe 21/22", wise21.Title);
    }
    [TestMethod]
    public async Task TestGetSemesters() {
        foreach (System.Net.Cookie cookie in Client.Client.CookieContainer.GetAllCookies())
            Console.Error.WriteLine($"Cookie: {cookie}");
        var all = await Client.Semesters.GetAllAsync(default).ToListAsync();
        Assert.IsTrue(all.Count > 20);
        Assert.IsTrue(all.Any(i => i.Id == Wise21Id));
    }
    [TestMethod]
    public async Task TestGetSelf() {
        var userId = await Client.Users.GetSelfAsync(default);
        var user = await userId.FetchAsync(default);
        Assert.AreEqual("czg6669", user.Username);
        Assert.AreEqual(user.Id, userId.Id);
        Console.WriteLine($"User {user.FormattedName} is logged in with permission {user.Permission}!");
    }
    [TestMethod]
    public async Task TestGetServerVersion() {
        var props = await Client.Studip.GetPropertiesAsync(default);
        var version = props.FirstOrDefault(i => i.Id == "studip-version" && i.Description == "Stud.IP-Version");
        Assert.IsNotNull(version);
        Console.WriteLine($"Version is {version.Value}!");
    }
    [TestMethod]
    public async Task TestGetRoutes() {
        var routes = await Client.GetAPIRoutesAsync(default);
        var usersroute = routes.FirstOrDefault(i => i.Pattern == "/v1/users");
        Assert.IsNotNull(usersroute);
        foreach (var route in routes)
            foreach (var method in route.Methods)
                Console.WriteLine($"{method} {route.Pattern}");
    }
    [TestMethod]
    public async Task TestDownloadFileLink() {
        var absFile = await Client.FileRefs["f840332b6307ee07349f2c457ccaafa5"].GetContentAsync(default);
        Assert.IsTrue(absFile is FileLink, "File was expected to be a redirection link!");
    }
    [TestMethod]
    public async Task TestDownloadFileBytes() {
        // PDF File should be 32357 bytes in size
        var absFile = await Client.FileRefs["0dffc63e8463b718e8ad6deac42173db"].GetContentAsync(default);
        if (absFile is not FileBytes fileBytes) { Assert.Fail("File was expected to be a redirection link!"); return; }
        var bytes = fileBytes.Content;
        Assert.AreEqual(32357, bytes.Length, "Incorrect file size, maybe a wrong file was downloaded!");
        Console.WriteLine($"File content was:\n{DebuggingMessageHandler.Truncate(Encoding.UTF8.GetString(bytes))}");
    }
    [TestMethod]
    public async Task TestActivities() {
        var activities = await (await Client.Users.GetSelfAsync(default)).GetActivitiesAsync(default).Take(20).ToListAsync();
        foreach (var activity in activities)
            Console.WriteLine($"[{activity.Date}] {activity.Title}: {activity.Content}");
    }
    [TestMethod]
    public async Task TestCalendar() {
        var calendar = await (await Client.Users.GetSelfAsync(default)).GetCalendarAsync(default);
        Console.WriteLine($"Content is: {calendar[..15].ReplaceLineEndings(string.Empty)}...{calendar[^16..].ReplaceLineEndings(string.Empty)}");
        Assert.IsTrue(calendar.TrimStart().StartsWith("BEGIN:VCALENDAR"), "Calendar file has wrong header!");
        Assert.IsTrue(calendar.TrimEnd().EndsWith("END:VCALENDAR"), "Calendar file has wrong trailer!");
    }

    private static readonly char[] Invalids = Path.GetInvalidFileNameChars();
    private static string SanitizeFile(string name) => string.Join("_", name.Split(Invalids, StringSplitOptions.RemoveEmptyEntries)).TrimEnd('.');

    [TestMethod]
    public async Task TestGetAllFiles() {
        var files = await Client.Courses["4d6618a24c6227d4c5b4154f533b73b3"].GetFileRefsAsync(default).ToListAsync();
        var folder = Path.GetTempPath();
        Console.WriteLine(folder);
        foreach (var file in files) {
            var path = ""; Assert.Fail("Folder path not implemented!");// Path.Combine(folder, Path.Combine(await Client.GetFolderPathAsync(file, default).Select(i => SanitizeFile(i.Name)).Reverse().ToArrayAsync()));
            Directory.CreateDirectory(path);
            var filename = SanitizeFile(file.FileName);
            var fullFileName = Path.Combine(path, filename);
            var dl = await Client.FileRefs[file.Id].GetContentAsync(default);
            await File.WriteAllBytesAsync(fullFileName, dl.Content);
            Console.WriteLine(fullFileName);
        }
    }

    private class Credentials {
        [JsonProperty("username", Required = Required.Always)] public string UserName { get; set; } = default!;
        [JsonProperty("password", Required = Required.Always)] public string Password { get; set; } = default!;
    }
}