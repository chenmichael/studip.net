﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Studip.NET.Tests;

public class DebuggingMessageHandler : DelegatingHandler {
    private const int TRUNCATE_LENGTH = 500;
    private const string TRUNCATE_INFO = "... Truncated!";

    public DebuggingMessageHandler(HttpMessageHandler handler) : base(handler) { }

    protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken) {
        Console.Error.WriteLine($"Req: {request.Method} {request.RequestUri}");
        foreach (var header in request.Headers)
            Console.Error.WriteLine($"\t{header.Key} = {string.Join(", ", header.Value)}");
        var response = await base.SendAsync(request, cancellationToken);
        var responseText = await response.Content.ReadAsStringAsync(cancellationToken);
        responseText = Truncate(responseText);
        Console.Error.WriteLine($"Rsp: {response.StatusCode}");
        foreach (var header in response.Headers)
            Console.Error.WriteLine($"\t{header.Key} = {string.Join(", ", header.Value)}");
        Console.Error.WriteLine(responseText);
        return response;
    }

    public static string Truncate(string responseText, int? truncateLength = null) {
        int length = truncateLength ?? TRUNCATE_LENGTH;
        responseText = responseText.Length > length ? responseText[..(length - TRUNCATE_INFO.Length)] + TRUNCATE_INFO : responseText;
        return responseText;
    }
}